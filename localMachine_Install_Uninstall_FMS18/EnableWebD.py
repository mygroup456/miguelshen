from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import os
import time

class Python(unittest.TestCase):
    def setUp(self):
        self.dir = os.path.dirname(os.path.abspath(__file__))
        self.filename = os.path.join(self.dir, 'chromedriver')
        self.driver = webdriver.Chrome(self.filename)
        self.driver.implicitly_wait(10)

    def test_enable_webdirect(self):
    #Open FAC in Google Chrome
        driver = self.driver
        with open('var_Host_Address.txt', 'r') as myfile:
   		host_address=myfile.read().replace('\n', '')
        driver.get("https://" + host_address + "/admin-console")
        
	time.sleep(2)
	
	# Login to FAC

	#Input FAC user name
	driver.find_element_by_xpath("//*[@id='inputUserName']").send_keys("a")
	time.sleep(2)
	#Input FAC password
	driver.find_element_by_xpath("//*[@id='inputPassword']").send_keys("a")
	time.sleep(2)
	#Click on "Sign In" button
	driver.find_element_by_xpath("//*[@id='LOGN_Butn_SignIn']").click()
	time.sleep(2)
	#Select "Don't use an SSL certificate" radio button
	driver.find_element_by_xpath("//*[@id='opt-1']").click()
	time.sleep(2)
	#Click on "Save" button
	driver.find_element_by_css_selector("body > app-root > app-onboarding > div.container-fluid.max-width.onboarding > div.body-one-column > button.btn.btn-submit").click()
	#Click on "I Accept the Risk" button in the "Don't Use an SSL Certificate" alert
	driver.find_element_by_css_selector("body > ngb-modal-window > div > div > div.modal-footer > button.btn.btn-outline-primary").click()

	#Click on "Connectors" tab header
	driver.find_element_by_xpath("//*[@id='HDR_Labl_Connectors']").click()
	time.sleep(2)
	#Click on "FileMaker WebDirect" toggle
	driver.find_element_by_xpath("//*[@id='CONN_BTN_TogWebDir']").click()
	time.sleep(3)
	#Click on "Master Machine" toggle
	#driver.find_element_by_xpath("//*[@id='CONN_BTN_TogWorker']").click()
	#time.sleep(2)

	#Click on "FileMaker Data API" subtab
	driver.find_element_by_xpath("//*[@id='CONN_Labl_FMDAPI']").click()
	#Click on "FileMaker Data API" toggle
	driver.find_element_by_xpath("//*[@id='CONN_BTN_TogFMDataApi']").click()
	time.sleep(2)

	#Click on "Plug-ins" subtab
    #driver.find_element_by_xpath("//*[@id='CONN_Labl_Plugins']").click()
	#Click on "FileMaker Script Engine Plug-ins" toggle
    #driver.find_element_by_xpath("//*[@id='CONN_BTN_TogFMSEPlugins']").click()
    #time.sleep(2)
	#Click on "Install Plug-in File Script Step" toggle for Server
    #driver.find_element_by_xpath("//*[@id='CONN_BTN_TogInstallPlugins']").click()
    #time.sleep(1)
	#Click on "Install Plug-in File Script Step" toggle for WebD
    #driver.find_element_by_xpath("//*[@id='CONN_BTN_TogWebSSPlugins']").click()
    #time.sleep(1)
	#Click "Yes" button to confirm toggling "Install Plug-in File Script Step" item
    #driver.find_element_by_id("CONF_Butn_Yes").click()

	#Click on "ODBC/JDBC" subtab
	driver.find_element_by_xpath("//*[@id='CONN_Labl_ODBCSource']").click()
	#Click on "Install Plug-in File Script Step" toggle for WebD
	driver.find_element_by_xpath("//*[@id='CONN_BTN_TogODBC']").click()
	time.sleep(2)

	#Click on "Configuration" tab header
	driver.find_element_by_xpath("//*[@id='HDR_Labl_Configuration']").click()
	time.sleep(2)
	#Click on "SSL Certificate" subtab
	driver.find_element_by_xpath("//*[@id='CONF_Link_SSLCertificate']").click()
	time.sleep(2)
	#Click Import Custom Certificate button
	driver.find_element_by_xpath("//*[@id='CONF_Link_ImportComCert']").click()
	time.sleep(2)
	#Click Signed Certificate File button
	driver.find_element_by_id("certificateFile").send_keys("/Users/prosqa_server1/Desktop/chaoyang/chaoyang_filemaker_com_cert.cer")
	time.sleep(2)
	#Click Private Key File button
	driver.find_element_by_id("privateKeyFile").send_keys("/Users/prosqa_server1/Desktop/chaoyang/chaoyang.filemaker.com.key")
	time.sleep(2)
	# Click Intermediate Certificate File button
	driver.find_element_by_id("intermediateCertificateFile").send_keys("/Users/prosqa_server1/Desktop/chaoyang/chaoyang_filemaker_com_interm.cer")
	time.sleep(2)
	#Input Private Key Password
	driver.find_element_by_xpath("//*[@id='CONF_Obj_PrivKeyPassTxtbx']").send_keys("Ha!rycrAb")
	#Click Import button to complete the Import Cert process
	driver.find_element_by_xpath("//*[@id='CONF_Butn_ImportCertImport']").click()
	#Dismiss dialog by clicking OK button
	driver.find_element_by_css_selector("body > ngb-modal-window > div > div > div.modal-footer > button:nth-child(1)").click()
	time.sleep(2)

	#Click on "Administration" tab header
	driver.find_element_by_xpath("//*[@id='HDR_Labl_Administration']").click()
	time.sleep(2)
	#Click on "External Authentication" subtab
	driver.find_element_by_xpath("//*[@id='ADM_Labl_ExternalAuth']").click()
	#Click Amazon drop-down toggle - Change button
	driver.find_element_by_xpath("//*[@id='CONF_Butn_AmazSettings']").click()
	time.sleep(2)
	#Input the Amazon Client ID & Client Secret
	driver.find_element_by_id("CONF_Obj_AmazClientIDTxtbx").send_keys("amzn1.application-oa2-client.bc42107d63d64e42804ba076b2303876")
	time.sleep(2)
	driver.find_element_by_id("CONF_Obj_AmazClientSecret").send_keys("3b8f52191a08de787600c48685bf7cc479bb04175e448fcb3d140f2b9db2a150")
	time.sleep(2)
	#Submit the change about Amazon account
	driver.find_element_by_id("CONF_Butn_AmazSaveAuthSett").click()
	time.sleep(2)
	#Click Google drop-down toggle - Change button
	driver.find_element_by_id("CONF_Butn_GoogSettings").click()
	time.sleep(2)
	#Input the Google Client ID & Client Secret
	driver.find_element_by_id("CONF_Obj_GoogClientIDTxtbx").send_keys("350604441554-kdjuh1h8k3mfbnsfq853pg8i8cikh7b8.apps.googleusercontent.com")
	time.sleep(2)
	driver.find_element_by_id("CONF_Obj_GoogClientSecret").send_keys("SpJV4B8xTFJAn68EsiWXTSUO")
	time.sleep(2)
	#Submit the change about Google account
	driver.find_element_by_id("CONF_Butn_GoogSaveAuthSett").click()
	time.sleep(2)
	#Click Microsoft drop-down toggle - Change button
	driver.find_element_by_id("CONF_Butn_MicrSettings").click()
	time.sleep(2)
	#Input the Azure Application ID, Azure Key & Azure Directory ID
	driver.find_element_by_id("CONF_Obj_MicrClientIDTxtbx").send_keys("a2dab30f-6fd2-41e0-b973-23fe21af3a40")
	time.sleep(2)
	driver.find_element_by_id("CONF_Obj_MicrClientSecret").send_keys("OxyUzkdBlDG9KIotQyNLxf6iLFcaAKah7VjV9mNA51k=")
	time.sleep(2)
	driver.find_element_by_id("CONF_Obj_MicrTenantIDTxtbx").send_keys("e7a22695-c32a-4fd0-b1b5-649825ed22ae")
	time.sleep(2)
	#Submit the change about Microsoft account
	driver.find_element_by_id("CONF_Butn_MicrSaveAuthSett").click()
	time.sleep(2)
	#Enable External Accounts under Admin Console Sign In section
	#driver.find_element_by_xpath("//*[@id='cbToggleExtAcct']").click()
	#time.sleep(2)
	#Enable FileMaker and External Server Accounts under Admin Console Sign In section
	driver.find_element_by_id("ExAuth_BTN_TogFMExtSrvr").click()
	time.sleep(2)
	#Enable sub-items - Amazon
	driver.find_element_by_id("ExAuth_BTN_TogAmaz").click()
	#Enable sub-items - Google
	driver.find_element_by_id("ExAuth_BTN_TogGoogl").click()
	#Enable sub-items - Microsoft
	driver.find_element_by_id("ExAuth_BTN_TogMicro").click()
	time.sleep(3)
	driver.find_element_by_xpath("// *[ @ id = 'HDR_Labl_Connectors']").click()
	time.sleep(2)
	driver.find_element_by_xpath("//*[@id='CONN_Labl_WebPublishing']").click()
	time.sleep(2)
	driver.find_element_by_xpath("// *[ @ id = 'CONN_BTN_TogWorker']").click()
	time.sleep(2)
	driver.find_element_by_xpath("/html/body/ngb-modal-window/div/div/div[3]/button[1]").click()
	time.sleep(2)
	driver.find_element_by_xpath("//*[@id='file']").send_keys("/Users/prosqa_server1/Downloads/OpenJDK8U-jre_x64_mac_hotspot_8u252b09.tar.gz")
	time.sleep(3)
	driver.find_element_by_xpath("/html/body/ngb-modal-window/div/div/div[3]/button").click()
	
	time.sleep(1)
	#Click on "Sign Out" button
	#driver.find_element_by_xpath("//*[@id='HDR_Link_SignInOut']").click()

    def tearDown(self):
      	driver = self.driver
        time.sleep(1)      
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()

