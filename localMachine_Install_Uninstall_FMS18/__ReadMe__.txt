[The purpose of the script is to install either latest or specified build of FileMaker Server on single macOS machines ]

0. Prerequisite
#The script requires Chrome browser and Selenium to run successfully(for enabling WebD and WPE, since there's no CLI or AdminAPI to enable it in 17, bug 224586).
Download and install Google Chrome from: 
https://www.google.com/chrome/

#Install brew - for simplification of installing components on macOS
#ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

#Install expect - shell script Silent_Install_FMS.sh is based on it
#brew install expect

Issue below command in Terminal to install Selenium:
sudo easy_install selenium


Part I - INSTALLATION

1. Fill up below variables in Set_variables.sh
	var_local_Password
	var_FMS_version (Default is "18")

2. Double click on _Install_FMS.command to automatically install latest(or specified) build of FileMaker Server, or run 'sh [Path]/_Install_FMS.sh'

3. The script installs latest build by default, if you want to install a specific build, open Set_variables.sh in text editor and uncomment line #19: #FMS_latest_build="xxx"

4. _Install_FMS.command/_Install_FMS.sh/Silent_Install_FMS.sh: Install FileMaker Server on the local machine.
EnableWebD.py: To sign in FAC and enable WebDirect and Web Publishing Engine in Connectors tab.

5. FileMaker Server admin account and password is a/a by default as specified in Assisted Install.txt.

6. After installation is done, the installer Mac_Installer_Server.zip will be left in the folder in case you may want to re-install the same version of FMS again.


Part II - UNINSTALLATION

1. Fill up the yourPassword and directoryPath for shell script uninstall.sh(default one is for FMS18)

2. Run _Unistall.sh to uninstall FMS


NOTE: 

these installation/uninstallation script is for FMS which is installed under the default directory ONLY!