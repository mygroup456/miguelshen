#!/bin/bash
echo "To check the all user connection:"
#User friendly function
/usr/bin/expect <<EOF
spawn fmsadmin disconnect client -m "The client will be disconnected in 30s due to maintenance"
expect "really disconnect client(s)? (y, n)"
send "y\r"
expect "username (admin)"
send "a\r"
expect "password"
send "a\r"

interact
expect eof
EOF


echo 'FileMaker123'| sudo -S command
cd /Library/Application\ Support/FileMaker/FileMaker\ Server/FileMaker\ Server\ 19\ Uninstaller.app/Contents/Resources/Scripts/Shell
sudo sh uninstall.sh

#Remove temp files
cd /Library/FileMaker\ Server
sudo rm -rfv /Library/FileMaker\ Server/CStore
sudo rm -rfv /Library/FileMaker\ Server/Admin
sudo rm -rfv /Library/FileMaker\ Server/Database\ Server
sudo rm -rfv /Library/FileMaker\ Server/HTTPServer
sudo rm -rfv /Library/FileMaker\ Server/Logs
sudo rm -rfv /Library/FileMaker\ Server/Web\ Publishing
