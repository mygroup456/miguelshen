from selenium import webdriver
import schedule
import shutil
import time
import os
import subprocess

def background_calculation():
    global build_no
    chrome_driver_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'chromedriver')
    driver = webdriver.Chrome(executable_path=chrome_driver_path)
    print('The version:' + str(build_no))
    clear()
    downloading = True
    status = True
    login_url = 'http://scm-san.filemaker.com/neon/Releases/FMS/19_Current/' + str(build_no) + '/Mac_Installer_Server.zip'

    driver.get(login_url)
    print("Now the FMS_Robot is working on download from %s" % (login_url))
    last_build = build_no
    while (not os.path.exists('/Users/prosqa_server1/Downloads/Mac_Installer_Server.zip')) and downloading and status:
        time.sleep(2)
        if os.path.isfile('/Users/prosqa_server1/Downloads/Mac_Installer_Server.zip.crdownload'):
            print('It is downloading now')
        elif os.path.isfile('/Users/prosqa_server1/Downloads/Mac_Installer_Server.zip'):
            downloading = False
            copy()
            driver.quit()
            subprocess.call(['/Users/prosqa_server1/Desktop/localMachine_Install_Uninstall_FMS18/_Uninstall.sh'])
            subprocess.call(['/Users/prosqa_server1/Desktop/localMachine_Install_Uninstall_FMS18/_Install_FMS.sh'])
            build_no = build_no + 1
        else:
            driver.quit()
            status = False
            print("FMS_Robot is working but failed to download from %s" % (login_url))
            #driver.get(login_url)
            #raise ValueError("%s isn't a file!" % '/Users/pengxiangshen/Desktop/autodownload/chromedriver')
    print("Next download work for build->" + str(build_no))
    driver.quit()


def clear():
    test = os.listdir('/Users/prosqa_server1/Downloads/')
    for item in test:
        if item.endswith(".zip"):
            os.remove(os.path.join('/Users/prosqa_server1/Downloads/', item))
        if item.endswith(".crdownload"):
            os.remove(os.path.join('/Users/prosqa_server1/Downloads/', item))
def copy():
    if os.path.isfile('/Users/prosqa_server1/Desktop/localMachine_Install_Uninstall_FMS18/Mac_Installer_Server.zip'):
        os.remove('/Users/prosqa_server1/Desktop/localMachine_Install_Uninstall_FMS18/Mac_Installer_Server.zip')
    shutil.copy('/Users/prosqa_server1/Downloads/Mac_Installer_Server.zip', '/Users/prosqa_server1/Desktop/localMachine_Install_Uninstall_FMS18')
    clear()

def automation():
    localTime = time.asctime(time.localtime(time.time()))
    msg = 'DownLoad Robot is Working...[%s]' % localTime.replace("  ", '')
    print(msg.center(116, '*'))
    background_calculation()

if __name__ == '__main__':
    clear()
    build_no = 104
    schedule.every(60).minutes.do(automation)   # schedule.every(1).hours.do(job) # schedule.every().day.at("10:30").do(job)
    keep_going = True
    while keep_going:
        schedule.run_pending()
        time.sleep(1)

